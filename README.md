# Test GitLab CI

This project demonstrates how to use GitLab CI to build _Castle Game Engine_ applications. See https://castle-engine.io/gitlab_ci for documentation, in short: just copy the `.gitlab-ci.yml` from this repo into your own.

Using [Castle Game Engine](https://castle-engine.io/).

## Building

Compile by:

- [CGE editor](https://castle-engine.io/manual_editor.php). Just use menu item _"Compile"_.

- Or use [CGE command-line build tool](https://castle-engine.io/build_tool). Run `castle-engine compile` in this directory.

- Or use [Lazarus](https://www.lazarus-ide.org/). Open in Lazarus `test_gitlab_ci_standalone.lpi` file and compile / run from Lazarus. Make sure to first register [CGE Lazarus packages](https://castle-engine.io/documentation.php).
